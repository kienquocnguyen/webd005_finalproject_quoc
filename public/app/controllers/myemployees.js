app.controller("myCtrl", function($scope, $http)
{
	// $scope.myId = "";
	// $scope.myFirst = "";
	// $scope.myLast = "";
	// $scope.myEmail = "";
	// $scope.myPhone = "";
	// $scope.myPosition = "";
    /*Show Data*/

	$http({
		method: 'GET',
		url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees'
	}).then(function successCallback(response){
		$scope.empployees = response.data;
	});

    /*End Show Data*/

    /*Add New*/

    $scope.myClick = function()
    {
	$http({
        url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees',
        method: 'POST',
        data: { 'name' : $scope.myName, 'email' : $scope.myEmail, 'contact_number' : $scope.myPhone, 'position' : $scope.myPosition }
    })
    .then(function successCallback(response) {
        $scope.empployees = response.data;
    }); 
    window.location.reload();
    };

    /*End Add New*/

    /*Edit*/

    $scope.editData = function(id)
    {
    $http({
        url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees/' + id,
        method: 'GET'
    })
    .then(function successCallback(response) {
        $scope.empployeesEdit = response.data;
    }); 
    };
    $scope.saveEdit = function(id)
    {
    $http({
        url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees/' + id,
        method: 'POST',
        data: $scope.empployeesEdit
    })
    .then(function successCallback(response) {
        location.reload();
    }); 
    };

    /*End Edit*/

    /*Pagination*/

    $http({
        method: 'GET',
        url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees_count'
    }).then(function successCallback(response){
        $scope.totalSlice = response.data;
        var i = 1;
        $scope.totalPage = Math.ceil($scope.totalSlice/5 + 1);
        $scope.pagingHtml = '';
        while(i < $scope.totalPage)
        {
            if(i == 1) {
                $scope.pagingHtml += '<li class="active"><a href="" ng-click="activePageData('+ i +')">' + i + '</a></li>';
            } else {
                $scope.pagingHtml += '<li><a href="" ng-click="activePageData('+i+')">' + i + '</a></li>';
            }
            i++;
        }
    });

    $scope.pageActive = 1;
    $scope.activePageData = function(p)
    {
        $scope.pageActive = p;
        $http({
            method: 'GET',
            url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees?page=' + p
        }).then(function successCallback(response){
            $scope.empployees = response.data;
        });
        $http({
        method: 'GET',
        url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees_count'
        }).then(function successCallback(response){
        $scope.totalSlice = response.data;
        var i = 1;
        $scope.totalPage = Math.ceil($scope.totalSlice/5);
        var currentPage = p;
        $scope.pagingHtml = '';
        while(i <= $scope.totalPage) 
        {
            if(i == currentPage) 
            {
                $scope.pagingHtml += '<li class="active"><a href="" ng-click="activePageData('+ i +')">' + i + '</a></li>';
            } 
            else 
            {
                $scope.pagingHtml += '<li><a href="" ng-click="activePageData('+i+')">' + i + '</a></li>';
            }
            i++;
        }
    });
    }
    $scope.nextpage = function()
    {
        $scope.pageActive++;
        $http({
            method: 'GET',
            url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees?page=' + $scope.pageActive
        }).then(function successCallback(response){
            $scope.empployees = response.data;
            var i = 1;
            $scope.pagingHtml = '';
            $scope.totalPage = Math.ceil($scope.totalSlice/5);
        while(i <= $scope.totalPage) 
        {
            if(i == $scope.pageActive) 
            {
                $scope.pagingHtml += '<li class="active"><a href="" ng-click="activePageData('+ i +')">' + i + '</a></li>';
            } 
            else 
            {
                $scope.pagingHtml += '<li><a href="" ng-click="activePageData('+i+')">' + i + '</a></li>';
            }
            i++;
        }
        });
    }
    $scope.previouspage = function()
    {
        $scope.pageActive--;
        $http({
            method: 'GET',
            url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees?page=' + $scope.pageActive
        }).then(function successCallback(response){
            $scope.empployees = response.data;
            var i = 1;
            $scope.pagingHtml = '';
            $scope.totalPage = Math.ceil($scope.totalSlice/5);
        while(i <= $scope.totalPage) 
        {
            if(i == $scope.pageActive) 
            {
                $scope.pagingHtml += '<li class="active"><a href="" ng-click="activePageData('+ i +')">' + i + '</a></li>';
            } 
            else 
            {
                $scope.pagingHtml += '<li><a href="" ng-click="activePageData('+i+')">' + i + '</a></li>';
            }
            i++;
        }
        });
    }
    /*End Pagination*/

    /*Delete*/
    
	$scope.myDelete = function (id)
    {
		$scope.employeeDeleted = id;
	};
	$scope.deleteEmployees = function(id)
    {
        $http({
        url: 'http://192.168.1.30:8080/angulara/public/api/v1/employees/' + id,
        method: 'DELETE'
        }).then(function successCallback(response) {
        $scope.empployees = response.data;
        }); 
        location.reload();
	};
    
    /*End Delete*/
});
app.directive("pagingDirective", function($compile) {
    return {
        link: function($scope, element){
            $scope.$watch("pagingHtml",function(newValue,oldValue) {
                element.html($compile(newValue)($scope));
            });
        }
    }
});
